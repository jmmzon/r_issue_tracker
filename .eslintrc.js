const path = require('path');

module.exports = {
  root: true,
  env: {
    node: true,
    'jest/globals': true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'import/extensions': 'off',
    'import/no-extraneous-dependencies': 'off',
    'no-param-reassign': 'off',
    camelcase: ['error', {
      allow: ['.*_id$', '.*_at$', 'first_name', 'last_name', 'password_confirmation', 'user_role'],
    }],
  },
  settings: {
    'import/resolver': {
      'babel-module': {
        alias: {
          '~client': path.resolve(__dirname, 'src', 'client'),
          '~server': path.resolve(__dirname, 'src', 'server'),
          '~common': path.resolve(__dirname, 'src', 'common'),
        },
      },
    },
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  plugins: ['jest'],
};
