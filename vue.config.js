require('express-async-errors');

const path = require('path');
// load webpack config from separate file, in order to support PhpStorm/Webstorm autocompletion
const webpackConfig = require('./webpack.config');
// const configureAPI = ;

module.exports = {
  configureWebpack: webpackConfig,
  outputDir: path.resolve(__dirname, 'dist', 'client'),
  // devServer:
};


if (process.env.NODE_ENV === 'development') {
  module.exports.devServer = {
    // eslint-disable-next-line global-require
    before: require('./src/server/configure.development'),
  };
}
