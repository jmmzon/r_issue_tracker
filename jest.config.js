module.exports = {
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/$1',
    '^~client(.*)$': '<rootDir>/src/client/$1',
    '^~common(.*)$': '<rootDir>/src/common/$1',
    '^vue$': 'vue/dist/vue.common.js',
  },
  moduleFileExtensions: ['js', 'vue', 'json'],
  transform: {
    '^.+\\.js$': 'babel-jest',
    '.*\\.(vue)$': 'vue-jest',
  },
  testMatch: [
    '**/client/**/*.spec.js|**/client/**/*.test.js',
  ],

};
