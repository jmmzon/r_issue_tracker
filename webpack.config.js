const path = require('path');

module.exports = {
  // overwrite default vue cli services entry point
  entry: {
    app: './src/client/main.js',
  },
  resolve: {
    alias: {
      '~client': path.resolve(__dirname, 'src', 'client'),
      '~server': path.resolve(__dirname, 'src', 'server'),
      '~common': path.resolve(__dirname, 'src', 'common'),
    },
  },
};
