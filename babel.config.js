const path = require('path');

module.exports = {
  presets: [
    '@vue/app',
  ],
  plugins: [
    [require.resolve('babel-plugin-module-resolver'), {
      alias: {
        '~server': path.resolve(__dirname, 'src', 'client'),
        '~common': path.resolve(__dirname, 'src', 'common'),
      },
    }],
  ],
};


if (process.env.NODE_ENV !== 'test') {
  module.exports.plugins.push('@babel/plugin-proposal-class-properties');
}
