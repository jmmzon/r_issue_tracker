# issue-tracker

Simple issue tracking system (basically todo app with task statuses)


## Project running

#### Run project for development
```
npm ci
npm run start:docker
npm run db:migrate:development
npm run serve
```
#### Run tests
```
npm run start:docker #if not started yet
npm run test:client
npm run test:server
```
**NOTE**: First run of server code tests displays error, because try to remove not existing test database  

#### Run linter
```
npm run lint
```


#### Run project on production 
```
# Copy .env.example to .env
cp .env.example .env

# Use your favourite editor to set required config options:
nano .env

# Build whole project
npm run build

# Migrate database
npm run db:migrate:production

# Start project
npm run start
```
**NOTE** It's recommend to use process manager like pm2, to restart server after crach

### Database tools

#### Create migration
```
npm sequelize migration:create --name migration_name
```

### Tech stack
- ExpressJS 4
- Vue.js
- MySQL + Seuquelize ORM
- Joi validation library
- Jest + supertest
- Webpack
- Docker

### Features
- creating issues
- grouping issues by status
- changing statuses of issues
- filtering of issues

### Incomprehension of the task
Starting this project i didnt focus on the core features. Instead I wrote user registration and authorization, 
project managing etc. (first). Then I realized I wouldnt be able to finish a project that 
would include tests on time, so I abandoned the original code and started to write a new one.  
The original one is available on **ABANDONED_first_attempt** branch.


### The future of the project
The project could be extended by feature like:

- sorting and pagination of isseues
- grouping issues by projects
- user registration and authorizaion
- assigning users to issues
- sending email notifications about new issues created or change of status 
- time tracking


### Development notes & todos
- a good idea would be to rethink transction handling mechanism. Now they are starting
automatically by middleware, and rollback on error, but in every /api route you must explicitly commit them
