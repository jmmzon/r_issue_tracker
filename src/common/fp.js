export function isFunction(fn) {
  return typeof fn === 'function';
}

export function not(fn) {
  return function negatedFn(...args) {
    return !fn(...args);
  };
}

export function groupBy(collection, predicate) {
  return collection.reduce((acc, value) => {
    acc[value[predicate]] = (acc[value[predicate]] || []).concat(value);
    return acc;
  }, {});
}

export function whereProp(prop, expectedValue) {
  return function wherePropComparator(obj) {
    return obj[prop] === expectedValue;
  };
}

export function keyBy(collection, predicate) {
  return collection.reduce((acc, value) => {
    acc[value[predicate]] = value;
    return acc;
  }, {});
}

export function isEmpty(obj) {
  if (Array.isArray(obj)) {
    return obj.length === 0;
  }
  return Object.values(obj).length === 0;
}
