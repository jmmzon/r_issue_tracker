export const STATUS_OPEN = 1;
export const STATUS_PENDING = 2;
export const STATUS_CLOSED = 3;

// fake status
export const STATUS_REMOVED = 4;

const allowedStatuesTransitions = {
  [STATUS_OPEN]: new Set([STATUS_PENDING, STATUS_REMOVED]),
  [STATUS_PENDING]: new Set([STATUS_CLOSED]),
  [STATUS_CLOSED]: new Set([]),
};


export function canChangeStatus(issue, newStatus) {
  return allowedStatuesTransitions[issue.status_id].has(newStatus);
}
