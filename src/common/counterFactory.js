export default function* makeCounter() {
  let count = 0;
  while (true) {
    yield count;
    count += 1;
  }
}
