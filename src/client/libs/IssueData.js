export class Builder {
  static from(obj) {
    return new Builder(obj);
  }

  constructor(obj) {
    this.obj = obj;
  }


  set(prop, value) {
    if (value) {
      this.obj[prop] = value;
    }
    return this;
  }
}

export class IssueData {
  id = null;

  title = '';

  description = '';

  status_id = null;

  constructor({
    id, title, description, status_id,
  } = {}) {
    Builder.from(this)
      .set('id', id)
      .set('title', title)
      .set('description', description)
      .set('status_id', status_id);
  }
}
