// vendors
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import VeeValidate from 'vee-validate';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue';
import 'vue2-animate/dist/vue2-animate.css';

import App from './App.vue';
import router from './router';
import store from './store';
import api from './libs/api';
import DefaultLayout from '~client/layouts/DefaultLayout';
import CenteredLayout from '~client/layouts/CenteredLayout';

Vue.prototype.$http = api;
Vue.use(BootstrapVue);
Vue.use(VeeValidate, {
  fieldsBagName: 'veeFields',
});

Vue.prototype.$http = api;
Vue.config.productionTip = false;

// import layouts
Vue.component('layout-default', DefaultLayout);
Vue.component('layout-centered', CenteredLayout);


new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
