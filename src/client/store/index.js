import Vue from 'vue';
import Vuex from 'vuex';
import api from '~client/libs/api';
import ui from './ui';

import { groupBy, keyBy, whereProp } from '~common/fp';
import { IssueData } from '~client/libs/IssueData';


Vue.use(Vuex);

export const getters = {
  filteredIssues(state) {
    return state.issues.filter(({ title = '', description = '' }) => {
      const titleMatch = title.toLowerCase().includes(state.filterValue);
      const descriptionMatch = description.toLowerCase().includes(state.filterValue);
      return titleMatch || descriptionMatch;
    });
  },
  issuesByStatus(state, storeGetters) {
    return groupBy(storeGetters.filteredIssues, 'status_id');
  },
  issuesByID(state) {
    return keyBy(state.issues, 'id');
  },
};

export const mutations = {
  SET_ISSUES(state, issues) {
    state.issues = issues;
  },
  SET_ISSUE(state, { ...issue }) {
    state.issue = issue;
  },
  ADD_ISSUE(state, issue) {
    state.issues.push(issue);
  },
  REMOVE_ISSUE(state, data) {
    const issueIndex = state.issues.findIndex(whereProp('id', data.id));
    if (issueIndex >= 0) {
      state.issues.splice(issueIndex, 1);
    }
  },
  REPLACE_ISSUE(state, data) {
    const issueIndex = state.issues.findIndex(whereProp('id', data.id));
    if (issueIndex >= 0) {
      state.issues.splice(issueIndex, 1, data);
    }
  },
  SET_ISSUE_PROP(state, { prop, value }) {
    state.issue[prop] = value;
  },
  SET_FILTER_VALUE(state, value = '') {
    state.filterValue = value.toLowerCase();
  },
};

export const actions = {
  async loadIssues({ commit }) {
    try {
      const { data: issues } = await api.get('/issues');
      commit('SET_ISSUES', issues);
      return issues;
    } catch (error) {
      throw error;
    }
  },
  async newIssue({ commit }) {
    commit('SET_ISSUE', new IssueData());
  },
  async createIssue({ commit, dispatch }, issueData) {
    try {
      const { data: issue } = await api.post('/issues', issueData);
      commit('ADD_ISSUE', issue);
      dispatch('ui/showSuccessNotification', `Issue #${issue.id} created`);
      return issue;
    } catch (error) {
      throw error;
    }
  },
  async loadIssue({ commit, state }, issueID) {
    const { ...issue } = state.issues.find(whereProp('id', issueID));
    commit('SET_ISSUE', issue);
  },
  async setClosed({ commit, dispatch }, issueID) {
    try {
      const { data: issue } = await api.post(`/issues/${issueID}/close`);
      commit('REPLACE_ISSUE', issue);
      dispatch('ui/showSuccessNotification', `Issue #${issueID} closed`);
      return issue;
    } catch (error) {
      throw error;
    }
  },
  async setPending({ commit, dispatch }, issueID) {
    try {
      const { data: issue } = await api.post(`/issues/${issueID}/setPending`);
      commit('REPLACE_ISSUE', issue);
      dispatch('ui/showSuccessNotification', `Issue #${issueID} set to pending`);
      return issue;
    } catch (error) {
      throw error;
    }
  },
  async removeIssue({ commit }, issueID) {
    try {
      await api.delete(`/issues/${issueID}`);
      commit('REMOVE_ISSUE', issueID);
    } catch (error) {
      throw error;
    }
  },
  setIssueProp({ commit }, { prop, value }) {
    commit('SET_ISSUE_PROP', { prop, value });
  },
  setFilterValue({ commit }, value) {
    commit('SET_FILTER_VALUE', value);
  },
};


export default new Vuex.Store({
  modules: { ui },
  state: {
    issues: [],
    issue: null,
    filterValue: '',
  },
  getters,
  mutations,
  actions,
});
