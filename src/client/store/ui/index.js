import makeCounter from '~common/counterFactory';
import { whereProp, not } from '~common/fp';

const counter = makeCounter();

export const initialState = {
  notifications: [],
};

export const mutations = {
  ADD_NOTIFICATION(state, notificationData) {
    state.notifications.push(notificationData);
  },
  REMOVE_NOTIFICATION(state, id) {
    state.notifications = state.notifications.filter(not(whereProp('id', id)));
  },
};

export const actions = {
  showNotification: ({ commit }, notificationData) => {
    const notification = {
      ...notificationData,
      id: counter.next().value,
    };
    commit('ADD_NOTIFICATION', notification);
    setTimeout(() => commit('REMOVE_NOTIFICATION', notification.id), 5000);
  },
  dismissNotification: ({ commit }, notificationID) => commit('REMOVE_NOTIFICATION', notificationID),
  showSuccessNotification({ dispatch }, message) {
    return dispatch('showNotification', {
      type: 'success',
      message,
    });
  },
  showErrorNotification({ dispatch }, message) {
    return dispatch('showNotification', {
      type: 'danger',
      message,
    });
  },
};


export default {
  namespaced: true,
  state: initialState,
  mutations,
  actions,
};
