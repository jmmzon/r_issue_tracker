import Vue from 'vue';
import Router from 'vue-router';
import Issues from '~client/views/Issues';

Vue.use(Router);

const router = new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Issues,
    },
  ],
});

export default router;
