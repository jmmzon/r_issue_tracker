import { Joi } from 'celebrate';

export default {
  body: {
    title: Joi.string().required(),
    description: Joi.string().optional().allow('').default(''),
  },
};
