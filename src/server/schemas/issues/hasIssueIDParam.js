import { Joi } from 'celebrate';

export default {
  params: {
    issueID: Joi.number().positive().required(),
  },
};
