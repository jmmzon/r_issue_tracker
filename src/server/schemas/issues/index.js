import changeStatus from './changeStatus';
import hasIssueIDParam from './hasIssueIDParam';
import update from './update';
import create from './create';

export default {
  changeStatus,
  hasIssueIDParam,
  update,
  create,
};
