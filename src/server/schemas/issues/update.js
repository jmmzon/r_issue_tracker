import { Joi } from 'celebrate';
import create from './create';

export default {
  ...create,
  params: {
    issueID: Joi.number().positive().required(),
  },
};
