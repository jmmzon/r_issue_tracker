import { Joi } from 'celebrate';

export default {
  params: {
    issueID: Joi.number().positive().required(),
    statusID: Joi.number().positive().valid(1, 2, 3).required(),
  },
};
