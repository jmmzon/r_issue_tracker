import bodyParser from 'body-parser';
import { errors } from 'celebrate';
import apiRoutes from './routes';
import exceptionErrorsHandler from '~server/middlewares/exceptionErrorsHandler';


export default function configureExpress(expressApp) {
  // pre route middlewares
  expressApp.use(bodyParser.json());

  // use api routes
  expressApp.use('/api', apiRoutes);

  //  post route middleware
  expressApp.use(exceptionErrorsHandler());
  expressApp.use(errors());
}
