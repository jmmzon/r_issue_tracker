
/**
 * @param {e.Errback} error
 * @param {AppRequest} req
 * @param {e.Response} res
 * @param {e.NextFunction} next
 */
export default async function rollbackTransactionOnErrorMiddleware(error, req, res, next) {
  if (error) {
    await req.app.transaction.rollback();
  }
  return next(error);
}
