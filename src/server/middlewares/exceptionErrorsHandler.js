import { isResponseError } from '~server/lib/errors/ResponseError';

export default function exceptionErrorsHandlerFactory() {
  return function exceptionErrorsHandlerMiddleware(err, req, res, next) {
    if (!isResponseError(err)) {
      return next(err);
    }

    return res.status(err.statusCode)
      .json(err)
      .send();
  };
}
