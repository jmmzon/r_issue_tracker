import db, { sequelize } from '~server/models';


/**
 * @typedef {e.Request} AppRequest
 *
 * @member {object} app.db
 * @property {sequelize.Transaction} app.transaction
 */

/**
 * @param {AppRequest} req
 * @param {e.Response} res
 * @param {e.NextFunction} next
 */
export default async function useDatabaseMiddleware(req, res, next) {
  req.app.db = db;
  req.app.transaction = await sequelize.transaction();
  next();
}
