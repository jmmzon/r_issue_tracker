import NotFoundError from '~server/lib/errors/NotFoundError';

export default class IssuesRepository {
  static async getIssueByID(app, issueID) {
    const issue = await app.db.IssueModel.findByPk(issueID, {
      transaction: app.transaction,
    });
    if (!issue) {
      throw new NotFoundError('Issue not found');
    }
    return issue;
  }
}
