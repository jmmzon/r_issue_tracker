import express from 'express';
import path from 'path';
import getConfig from './config';
import app from './app';

app.use(express.static(path.resolve(__dirname, '../client')));

app.listen(getConfig('app').port);
