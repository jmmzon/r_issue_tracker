require('dotenv').config();

module.exports = {
  production: {
    port: process.env.APP_PORT,
  },
};
