import database from './database';
import app from './app';

const configModules = {
  database,
  app,
};

const env = process.env.NODE_ENV;

export default function getConfig(configName) {
  if (!configModules[configName]) {
    throw new Error(`Unknown config name ${configName}`);
  } else {
    return configModules[configName][env];
  }
}
