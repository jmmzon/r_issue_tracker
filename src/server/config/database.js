require('dotenv').config();

module.exports = {
  development: {
    username: 'issue_tracker',
    password: 'issue_tracker',
    database: 'issue_tracker',
    host: '127.0.0.1',
    port: '3408',
    dialect: 'mysql',
    logging: false,
  },
  test: {
    username: 'root',
    password: '',
    database: 'issue_tracker_test',
    host: '127.0.0.1',
    port: '3407',
    dialect: 'mysql',
    logging: false,
  },
  production: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT || 3306,
    dialect: 'mysql',
    logging: false,
  },
};
