// import vendors
import Sequelize from 'sequelize';
import config from '~server/config/database';
// import helpers functions
// import AppModels
import IssueModel from './IssueModel';
import IssuesStatusModel from './IssueStatusModel';
import { isFunction } from '~common/fp';

export { Sequelize };

// setup sequelize config
export const env = process.env.NODE_ENV || 'development';
export const sequelize = new Sequelize(
  config[env].database,
  config[env].username,
  config[env].password,
  config[env],
);

const AppModels = {
  IssueModel,
  IssuesStatusModel,
};

// initialize AppModels
Object.values(AppModels)
  .forEach((model) => {
    model.init(sequelize, Sequelize);
  });

// create associations
Object.values(AppModels)
  .filter(({ associate }) => isFunction(associate))
  .forEach(model => model.associate(AppModels));
// export AppModels
export {
  IssueModel,
  IssuesStatusModel,
};

export default AppModels;
