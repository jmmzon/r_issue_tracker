/* eslint-disable */

import * as Sequelize from 'sequelize';

export default class IssueStatusModel extends Sequelize.Model {
    static init(sequelize, DataTypes) {
        return super.init({
            id: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                primaryKey: true,
                autoIncrement: true,
            },
            name: {
                type: DataTypes.STRING(80),
                allowNull: false,
            },
            created_at: DataTypes.DATE,
            updated_at: DataTypes.DATE,
        }, {
            sequelize,
            tableName: 'issue_statuses',
            underscored: true,
            modelName: 'Status'
        });
    }

    static associate(models) {
        this.hasMany(models.IssueModel, {
            foreignKey: 'status_id'
        });
    }
}
