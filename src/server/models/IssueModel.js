import * as Sequelize from 'sequelize';

import InvalidRequest from '~server/lib/errors/InvalidRequest';
import {
  canChangeStatus, STATUS_CLOSED, STATUS_PENDING, STATUS_REMOVED,
} from '~common/lib/statuses';

export default class IssueModel extends Sequelize.Model {
  static init(sequelize, DataTypes) {
    return super.init({
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        primaryKey: true,
        autoIncrement: true,
      },
      status_id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        references: {
          model: 'issue_statuses',
          key: 'id',
        },
      },
      title: {
        type: DataTypes.STRING(80),
        allowNull: false,
      },
      description: {
        type: DataTypes.TEXT,
        allowNull: false,
        defaultValue: '',
      },
      created_at: DataTypes.DATE,
      updated_at: DataTypes.DATE,
    }, {
      sequelize,
      tableName: 'issues',
      underscored: true,
      modelName: 'Issue',
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    });
  }

  static associate(models) {
    this.belongsTo(models.IssuesStatusModel, {
      foreignKey: 'status_id',
    });

    this.addScope(
      'defaultScope',
      {
        attributes: ['id', 'title', 'description', 'status_id', 'created_at', 'updated_at'],
        include: [
          {
            attributes: ['id', 'name'],
            model: models.IssuesStatusModel,
          },
        ],
      },
      {
        override: true,
      },
    );
  }

  set status_id(status) {
    if (!canChangeStatus(this, status)) {
      throw new InvalidRequest();
    }

    this.setDataValue('status_id', status);
  }

  allowClose() {
    return canChangeStatus(this, STATUS_CLOSED);
  }

  allowPending() {
    return canChangeStatus(this, STATUS_PENDING);
  }

  allowRemove() {
    return canChangeStatus(this, STATUS_REMOVED);
  }
}
