/* eslint-disable */

import * as Sequelize from 'sequelize';

export default class SampleModel extends Sequelize.Model {
  static init(sequelize, DataTypes) {
    return super.init({}, {
      sequelize,
      tableName: '',
      underscored: true,
    });
  }

  static associate(models) {

  }
}
