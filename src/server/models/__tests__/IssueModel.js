import { IssueModel } from '~server/models';

describe('IssueModel test', () => {
  it('create IssueModel instance', () => {
    const issueModel = IssueModel.build({
      title: 'Jan',
      description: 'Zon',
    });

    expect(issueModel).toMatchSnapshot();
  });
});
