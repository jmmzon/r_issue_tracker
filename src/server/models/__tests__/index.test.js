import { sequelize } from '..';

describe('test sequelize database connection', async () => {
  beforeAll(() => {

  });
  it('should connect succesfully to database', async () => {
    expect.assertions(1);

    await sequelize.authenticate()
      .then((result) => {
        expect(result)
          .toBe(undefined);
      });
  });


  it('should create database', async () => {
    const rows = await sequelize.query('CREATE DATABASE IF NOT EXISTS tests');
    expect(rows.length).toEqual(2);
  });

  it('should drop database', async () => {
    const rows = await sequelize.query('DROP DATABASE IF EXISTS tests');
    expect(rows.length).toEqual(2);
  });


  afterAll(() => {
    sequelize.close();
  });
});
