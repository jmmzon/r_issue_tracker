import ResponseError from '~server/lib/errors/ResponseError';

export default class NotFoundError extends ResponseError {
  name = 'NotFoundError';

  constructor(message) {
    super(message, 404);
  }
}
