import ResponseError from '~server/lib/errors/ResponseError';

export default class NotImplementedError extends ResponseError {
  name = 'NotImplementedError';

  constructor(message) {
    super(message, 404);
  }
}
