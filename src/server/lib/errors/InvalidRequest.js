import ResponseError from '~server/lib/errors/ResponseError';

export default class InvalidRequest extends ResponseError {
  name = 'InvalidRequest';

  constructor(message) {
    super(message, 400);
  }
}
