export default class ResponseError extends Error {
  name = 'ResponseError';

  statusCode;

  message;

  constructor(message, statusCode = 500) {
    super();

    this.message = message;
    this.statusCode = statusCode;

    Object.defineProperty(this, 'isResponseError', {
      enumerable: false,
      value: true,
    });
  }
}

export function isResponseError(err) {
  return err instanceof ResponseError;
}
