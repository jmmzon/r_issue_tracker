const isDev = process.env.NODE_ENV === 'development';
const isTest = process.env.NODE_ENV === 'test';


const config = {
  presets: [
    ['@babel/env', {
      exclude: ['transform-regenerator'],
      targets: {
        node: 'current',
      },
    }],
  ],
  only: [
    '../server',
    '../common',
    './src/common',
    './src/server',
  ],
  plugins: [
    [require.resolve('babel-plugin-module-resolver'), {
      alias: {
        '~server': isTest ? '.' : './src/server',
        '~common': isTest ? '../common' : './src/common',
      },
    }],
    '@babel/plugin-proposal-class-properties',
  ],
};

module.exports = config;

if (isDev) {
  config.configFile = false;
}
