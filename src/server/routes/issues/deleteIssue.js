/**
 * @param {AppRequest} req
 * @param res
 * @returns {*}
 */
import IssuesRepository from '~server/repositories/IssuesRepository';
import InvalidRequest from '~server/lib/errors/InvalidRequest';

export default async function deleteIssue(req, res) {
  const { issueID } = req.params;

  const issue = await IssuesRepository.getIssueByID(req.app, issueID);

  if (!issue.allowRemove()) {
    throw new InvalidRequest(`Cannot remove issue with status: ${issue.Status.name}`);
  }

  await issue.destroy({ transaction: req.app.transaction });

  await req.app.transaction.commit();
  res.send(issue);
}
