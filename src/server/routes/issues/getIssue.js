import IssuesRepository from '~server/repositories/IssuesRepository';

/**
 * @param {AppRequest} req
 * @param res
 * @returns {*}
 */
export default async function getIssue(req, res) {
  const { issueID } = req.params;
  const issue = await IssuesRepository.getIssueByID(req.app, issueID);

  await req.app.transaction.commit();
  res.send(issue);
}
