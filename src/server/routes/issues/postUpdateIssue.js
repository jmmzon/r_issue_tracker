import { STATUS_OPEN } from '~common/lib/statuses';
import IssuesRepository from '~server/repositories/IssuesRepository';
import InvalidRequest from '~server/lib/errors/InvalidRequest';

/**
 * @param {AppRequest} req
 * @param res
 * @returns {*}
 */
export default async function postUpdateIssue(req, res) {
  const {
    params: { issueID },
    body: issueData,
  } = req;

  const issue = await IssuesRepository.getIssueByID(req.app, issueID);

  if (issue.status_id !== STATUS_OPEN) {
    throw new InvalidRequest('Cannot update closed or pending issue');
  }

  issue.title = issueData.title;
  issue.description = issueData.description;

  await issue.save({ transaction: req.app.transaction });

  await req.app.transaction.commit();
  return res.send(issue);
}
