/**
 * @param {AppRequest} req
 * @param res
 * @returns {*}
 */
import IssuesRepository from '~server/repositories/IssuesRepository';
import { STATUS_PENDING } from '~common/lib/statuses';
import InvalidRequest from '~server/lib/errors/InvalidRequest';

export default async function postSetIssuePending(req, res) {
  const { issueID } = req.params;
  const issue = await IssuesRepository.getIssueByID(req.app, issueID);

  if (!issue.allowPending()) {
    throw new InvalidRequest('Only open issues can be set to pending');
  }

  issue.status_id = STATUS_PENDING;
  await issue.save({ transaction: req.app.transaction });

  await req.app.transaction.commit();
  res.send({
    ...issue.get(),
    Status: {
      id: 2,
      name: 'Pending',
    },
  });
}
