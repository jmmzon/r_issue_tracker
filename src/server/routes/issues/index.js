import express from 'express';
import getIssue from '~server/routes/issues/getIssue';
import getIssues from '~server/routes/issues/getIssues';
import postCreateIssue from '~server/routes/issues/postCreateIssue';
import postUpdateIssue from '~server/routes/issues/postUpdateIssue';
import deleteIssue from '~server/routes/issues/deleteIssue';
import postCloseIssue from '~server/routes/issues/postCloseIssue';
import postSetIssuePending from '~server/routes/issues/postSetIssuePending';

import issueSchemas from '~server/schemas/issues';
import { celebrate } from 'celebrate';

const issuesRouter = express.Router();


issuesRouter.get('/', getIssues);
issuesRouter.get('/:issueID',
  celebrate(issueSchemas.hasIssueIDParam),
  getIssue);

issuesRouter.post('/',
  celebrate(issueSchemas.create, { stripUnknown: true }),
  postCreateIssue);

issuesRouter.post('/:issueID',
  celebrate(issueSchemas.update, { stripUnknown: true }),
  postUpdateIssue);

issuesRouter.post('/:issueID/close',
  celebrate(issueSchemas.hasIssueIDParam),
  postCloseIssue);

issuesRouter.post('/:issueID/setPending',
  celebrate(issueSchemas.hasIssueIDParam),
  postSetIssuePending);

issuesRouter.delete('/:issueID', deleteIssue);


export default issuesRouter;
