/**
 * @param {AppRequest} req
 * @param res
 * @returns {*}
 */
import { STATUS_OPEN } from '~common/lib/statuses';
import IssuesRepository from '~server/repositories/IssuesRepository';

export default async function postCreateIssue(req, res) {
  const { body: issueData } = req;

  const { id } = await req.app.db.IssueModel.create({
    title: issueData.title,
    description: issueData.description,
    status_id: STATUS_OPEN,
  }, {
    transaction: this.transaction,
  });

  const issue = await IssuesRepository.getIssueByID(req.app, id);

  await req.app.transaction.commit();
  return res.send(issue);
}
