import app from '~server/app';
import request from 'supertest';
import { sequelize } from '~server/models';

const expectedFileResult = {
  id: 1,
  title: 'Issue 1',
  description: 'Issue description',
  status_id: 1,
  created_at: expect.any(String),
  updated_at: expect.any(String),
  Status: {
    id: 1,
    name: 'Opened',
  },
};

const expectedFileResultAfterUpdate = {
  ...expectedFileResult,
  title: 'Issue 1 - renamed',
  description: 'Issue description - changed',
};

describe('api/issues endpoints test', () => {
  afterAll(async () => {
    await sequelize.close();
  });

  it('GET /api/issues server should response with empty array to GET /api/issues if no records is insert', async () => {
    const response = await request(app).get('/api/issues');
    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual([]);
  });

  it('POST /api/issues server should create new issues and return it to POST /api/issues & fetch it back', async () => {
    const issue = {
      title: 'Issue 1',
      description: 'Issue description',
    };

    let response = await request(app).post('/api/issues').send(issue);
    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual(expectedFileResult);

    response = await request(app).get(`/api/issues/${response.body.id}`);
    expect(response.body).toEqual(expectedFileResult);
    expect(response.statusCode).toBe(200);
  });

  it('POST /api/issues/:id should update issue if is open', async () => {
    const updateData = {
      title: 'Issue 1 - renamed',
      description: 'Issue description - changed',
    };
    const response = await request(app).post(`/api/issues/${expectedFileResult.id}`).send(updateData);
    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual(expectedFileResultAfterUpdate);
  });

  it('should return error, when try GET file that not exists', async () => {
    const response = await request(app).get('/api/issues/10');
    expect(response.statusCode).toBe(404);
    expect(response.body).toEqual({
      message: 'Issue not found',
      name: 'NotFoundError',
      statusCode: 404,
    });
  });

  it('server should response with array of created issues (1) to GET /api/issues if no records is insert', async () => {
    const response = await request(app).get('/api/issues');
    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual(expect.any(Array));
    expect(response.body.length).toEqual(1);
    expect(response.body).toEqual([expectedFileResultAfterUpdate]);
  });

  it('should return error, when try to close open(not pending) issue', async () => {
    const response = await request(app).post(`/api/issues/${expectedFileResultAfterUpdate.id}/close`);
    expect(response.statusCode).toEqual(400);
    expect(response.body).toEqual({
      name: 'InvalidRequest',
      statusCode: 400,
      message: 'Only pending issues can be closed',
    });
  });

  it('should set issue pending state', async () => {
    const response = await request(app).post(`/api/issues/${expectedFileResult.id}/setPending`);
    expect(response.statusCode).toEqual(200);
    expect(response.body).toEqual({
      ...expectedFileResultAfterUpdate,
      status_id: 2,
      Status: {
        id: 2,
        name: 'Pending',
      },
    });
  });

  it('POST /api/issues/:id should return error if file dont have status opened', async () => {
    const updateData = {
      title: 'Issue 1 - renamed again (or not?)',
      description: 'Issue description - changed',
    };
    const response = await request(app).post(`/api/issues/${expectedFileResult.id}`).send(updateData);
    expect(response.statusCode).toBe(400);
    expect(response.body).toEqual({
      message: 'Cannot update closed or pending issue',
      name: 'InvalidRequest',
      statusCode: 400,
    });
  });

  it('should close pending issue', async () => {
    const response = await request(app).post(`/api/issues/${expectedFileResult.id}/close`);
    expect(response.statusCode).toEqual(200);
    expect(response.body).toEqual({
      ...expectedFileResultAfterUpdate,
      status_id: 3,
      Status: {
        id: 3,
        name: 'Closed',
      },
    });
  });

  it('should return error, when try set task pending, if it is closed', async () => {
    const response = await request(app).post(`/api/issues/${expectedFileResult.id}/setPending`);
    expect(response.statusCode).toEqual(400);
    expect(response.body).toEqual({
      name: 'InvalidRequest',
      statusCode: 400,
      message: 'Only open issues can be set to pending',
    });
  });

  it('should fail remove issue if its not open', async () => {
    const response = await request(app).delete(`/api/issues/${expectedFileResult.id}`);
    expect(response.statusCode).toEqual(400);
    expect(response.body).toEqual({
      message: 'Cannot remove issue with status: Closed',
      name: 'InvalidRequest',
      statusCode: 400,
    });
  });


  it('should create and next remove issue', async () => {
    await request(app).post('/api/issues').send(expectedFileResult);
    let response = await request(app).get('/api/issues');
    expect(response.statusCode).toBe(200);
    expect(response.body.length).toBe(2);

    response = await request(app).delete('/api/issues/2');
    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual({
      ...expectedFileResult,
      id: 2,
    });

    response = await request(app).get('/api/issues/2');
    expect(response.statusCode).toBe(404);
    expect(response.body).toEqual({
      message: 'Issue not found',
      name: 'NotFoundError',
      statusCode: 404,
    });
  });
});
