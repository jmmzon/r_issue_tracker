/**
 * @param {AppRequest} req
 * @param res
 * @returns {*}
 */
import IssuesRepository from '~server/repositories/IssuesRepository';
import { STATUS_CLOSED } from '~common/lib/statuses';
import InvalidRequest from '~server/lib/errors/InvalidRequest';

export default async function postCloseIssue(req, res) {
  const { issueID } = req.params;
  const issue = await IssuesRepository.getIssueByID(req.app, issueID);

  if (!issue.allowClose()) {
    throw new InvalidRequest('Only pending issues can be closed');
  }

  issue.status_id = STATUS_CLOSED;
  await issue.save({ transaction: req.app.transaction });

  await req.app.transaction.commit();
  res.send({
    ...issue.get(),
    Status: {
      id: 3,
      name: 'Closed',
    },
  });
}
