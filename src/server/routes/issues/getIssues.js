/**
 * @param {AppRequest} req
 * @param res
 * @returns {*}
 */
export default async function getIssues(req, res) {
  const issues = await req.app.db.IssueModel.findAll({
    transaction: req.app.transaction,
  });

  await req.app.transaction.commit();
  res.send(issues);
}
