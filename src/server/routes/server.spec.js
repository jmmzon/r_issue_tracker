import request from 'supertest';
import app from '~server/app';
import { sequelize } from '~server/models';

describe('/api/server/ routes test', () => {
  afterAll(() => {
    sequelize.close();
  });

  it('server should response to GET request to /api/server/status', async () => {
    const response = await request(app).get('/api/server/status');
    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual({ ok: true });
  });

  it('server should response with error to POST to /api/server/error', async () => {
    const response = await request(app).get('/api/server/error');
    expect(response.statusCode).toBe(500);
    expect(response.body).toEqual({
      message: 'Standard ResponseError',
      name: 'ResponseError',
      statusCode: 500,
    });
  });
});
