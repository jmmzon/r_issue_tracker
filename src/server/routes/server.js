import express from 'express';
import ResponseError from '~server/lib/errors/ResponseError';

const router = express.Router();

router.get('/status', async (req, res) => {
  res.send({ ok: true });
  await req.app.transaction.commit();
});

router.get('/error', async () => {
  throw new ResponseError('Standard ResponseError');
});

export default router;
