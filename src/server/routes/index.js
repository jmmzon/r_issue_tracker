import express from 'express';
import NotFoundError from '~server/lib/errors/NotFoundError';
// import routes
import server from './server';
import issuesRouter from '~server/routes/issues';
import useDatabaseMiddleware from '~server/middlewares/useDatabaseMiddleware';
import rollbackTransactionOnErrorMiddleware
  from '~server/middlewares/rollbackTransactionOnErrorMiddleware';

/**
 * @type {Router}
 */
const router = express.Router();

router.use(useDatabaseMiddleware);
router.use('/server', server);
router.use('/issues', issuesRouter);

router.use('*', () => {
  throw new NotFoundError('route not found');
});
router.use(rollbackTransactionOnErrorMiddleware);


export default router;
