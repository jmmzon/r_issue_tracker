import 'express-async-errors';
import express from 'express';
import configure from '~server/configure.production';

const app = express();

configure(app);

export default app;
