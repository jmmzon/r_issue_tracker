
import { actions, initialState, mutations } from '~client/store/ui/index';
import '~common/counterFactory';


jest.mock('~common/counterFactory', () => function* counter() {
  while (true) {
    yield 1;
  }
});
let state;
let context;

beforeEach(() => {
  state = {
    notifications: [
      {
        id: 12,
        message: 'Error message',
        type: 'error',
      },
    ],
  };
  context = {
    commit: jest.fn(),
    dispatch: jest.fn(),
    state,
  };
});


it('should initial state match snapshot', () => {
  expect(initialState).toMatchSnapshot();
});

describe('ui store: actions tests', () => {
  it('showNotification action should add notification with generated id & commit ADD_NOTIFICATION & set clear timeout', () => {
    jest.useFakeTimers();

    const notification = {
      message: 'Notification message',
      type: 'success',
    };

    actions.showNotification(context, notification);
    expect(context.commit).toBeCalledWith('ADD_NOTIFICATION', {
      ...notification,
      id: 1,
    });
    expect(setTimeout).toBeCalledWith(expect.any(Function), 5000);
    expect(context.commit).toHaveBeenCalledTimes(1);
    jest.advanceTimersByTime(5000);
    expect(context.commit).toBeCalledWith('REMOVE_NOTIFICATION', 1);
  });

  it('dismissNotification action: should commit REMOVE_NOTIFICATION', async () => {
    await actions.dismissNotification(context, 1);
    expect(context.commit).toBeCalledWith('REMOVE_NOTIFICATION', 1);
  });

  it('showSuccessNotification action: should dispatch showNotification action with message and success type', () => {
    actions.showSuccessNotification(context, 'New message');
    expect(context.dispatch).toBeCalledWith('showNotification', {
      message: 'New message',
      type: 'success',
    });
  });

  it('showErrorNotification action: should dispatch showNotification action with message and error type', () => {
    actions.showSuccessNotification(context, 'New message');
    expect(context.dispatch).toBeCalledWith('showNotification', {
      message: 'New message',
      type: 'success',
    });
  });
});

describe('ui store: mutations tests', () => {
  it('ADD_NOTIFICATION mutation: should append notifications at end', () => {
    const notification = {
      id: 14,
      message: 'Notification message',
      type: 'success',
    };
    mutations.ADD_NOTIFICATION(state, notification);
    expect(state.notifications.length).toBe(2);
    expect(state.notifications[1]).toBe(notification);
  });

  it('REMOVE_NOTIFICATION: should remove notification with passed id', () => {
    const notificationID = 12;
    mutations.REMOVE_NOTIFICATION(state, notificationID);
    expect(state.notifications.length).toBe(0);
  });
});
