
import { getters, mutations, actions } from '~client/store';
import api from '~client/libs/api/index';
import { IssueData } from '~client/libs/IssueData';

jest.mock('~client/libs/api', () => ({
  get: jest.fn(() => Promise.resolve({ data: {} })),
  post: jest.fn(() => Promise.resolve({ data: {} })),
  delete: jest.fn(() => Promise.resolve({ data: {} })),
}));


let state;
let context;

beforeEach(() => {
  state = {
    issues: [
      {
        id: 12,
        title: 'Issue 1',
        description: 'Issue 2',
        status_id: 1,
      },
      {
        id: 13,
        title: 'Issue 2',
        description: 'Issue 2',
        status_id: 2,
      },
      {
        id: 14,
        title: 'Issue 3',
        description: 'Issue 1 related',
        status_id: 3,
      },
      {
        id: 15,
        title: 'Issue 4',
        description: 'Issue 2',
        status_id: 3,
      },
    ],
    issue: {
      id: 13,
      title: 'Issue 1',
      description: 'Issue 2',
      status_id: 2,
    },
    filterValue: '',
  };

  context = {
    commit: jest.fn(),
    dispatch: jest.fn(),
    state,
  };
});


describe('index store: actions test', () => {
  it('loadIssues should load issues from api & commit mutation', async () => {
    const issues = state.issues.slice();
    api.get.mockResolvedValue({ data: issues });
    const loadedIssues = await actions.loadIssues(context);
    expect(api.get).toBeCalledWith('/issues');
    expect(context.commit).toHaveBeenCalledWith('SET_ISSUES', issues);
    expect(loadedIssues).toBe(issues);
  });

  it('loadIssues should throw error/reject promise  on request failure', async () => {
    api.get.mockRejectedValue(new Error('Request error'));
    expect(api.get).toBeCalledWith('/issues');
  });

  it('setClosed action should post issue to api & commit putation & dispatch notification action', async () => {
    const issue = state.issues[0];
    api.post.mockResolvedValue({ data: { ...issue, status_id: 2 } });
    const closedIssue = await actions.setClosed(context, issue.id);

    expect(api.post).toBeCalledWith(`/issues/${issue.id}/close`);
    expect(context.commit).toBeCalledWith('REPLACE_ISSUE', closedIssue);
    expect(context.dispatch).toBeCalledWith('ui/showSuccessNotification', `Issue #${issue.id} closed`);
  });

  it('newIssue should commit SET_ISSUE with new IssueData', () => {
    actions.newIssue(context);
    expect(context.commit).toBeCalledWith('SET_ISSUE', expect.any(IssueData));
  });

  it('setPending action should post issue to api & commit putation & dispatch notification action', async () => {
    const issue = state.issues[0];
    api.post.mockResolvedValue({ data: { ...issue, status_id: 1 } });
    const setPending = await actions.setPending(context, issue.id);

    expect(api.post).toBeCalledWith(`/issues/${issue.id}/setPending`);
    expect(context.commit).toBeCalledWith('REPLACE_ISSUE', setPending);
    expect(context.dispatch).toBeCalledWith('ui/showSuccessNotification', `Issue #${issue.id} set to pending`);
  });

  it('setIssueProp should commit mutation', () => {
    const updateData = { prop: 'prop', value: 'value' };
    actions.setIssueProp(context, updateData);
    expect(context.commit).toBeCalledWith('SET_ISSUE_PROP', updateData);
  });

  it('createIssue should post issue data to api & commit ADD_ISSUE mutation & dispatch notification action', async () => {
    const issue = {
      ...state.issues[0],
      id: 12,
    };
    api.post.mockResolvedValue({ data: issue });

    const createdIssue = await actions.createIssue(context, issue);

    expect(api.post).toBeCalledWith('/issues', issue);
    expect(context.commit).toBeCalledWith('ADD_ISSUE', issue);
    expect(context.dispatch).toBeCalledWith('ui/showSuccessNotification', 'Issue #12 created');
    expect(createdIssue).toEqual(issue);
  });

  it('should load issue from issues', async () => {
    const issueId = state.issues[0].id;
    await actions.loadIssue(context, issueId);
    expect(context.commit).toBeCalledWith('SET_ISSUE', state.issues[0]);
  });

  it('removeIssue: should send delete request to api & commit REMOVE_ISSUE mutation', async () => {
    const issueID = state.issues[0].id;
    await actions.removeIssue(context, issueID);
    expect(api.delete).toBeCalledWith(`/issues/${issueID}`);
    expect(context.commit).toBeCalledWith('REMOVE_ISSUE', issueID);
  });

  it('setFilterValue: should dispatch SET_FILTER_VALUE mutation', () => {
    actions.setFilterValue(context, 'filter value', () => {
      expect(context.commit).toBeCalledWith('SET_FILTER_VALUE', 'filter value');
    });
  });
});


describe('index store getters tests', () => {
  it('should issuesByStatus return statuses grouped by status_id', () => {
    const gettersMock = {
      filteredIssues: state.issues,
    };
    expect(getters.issuesByStatus(state, gettersMock)).toEqual({
      1: [state.issues[0]],
      2: [state.issues[1]],
      3: [state.issues[2], state.issues[3]],
    });
  });

  it('should issuesByID getter return statuses grouped by id', () => {
    expect(getters.issuesByID(state)).toEqual({
      12: state.issues[0],
      13: state.issues[1],
      14: state.issues[2],
      15: state.issues[3],
    });
  });

  it('should filteredIssues getter return list of filtered issues', () => {
    state.filterValue = 'issue 1';
    expect(getters.filteredIssues(state)).toEqual([
      state.issues[0],
      state.issues[2],
    ]);
  });
});

describe('index store: mutations tests', () => {
  it('should ADD_ISSUE mutation add item it issues', () => {
    const issue = {
      id: 19,
      title: 'Super test',
    };

    mutations.ADD_ISSUE(state, issue);

    expect(state.issues.length).toBe(5);
    expect(state.issues[4]).toBe(issue);
  });

  it('should REPLACE_ISSUE mutation replace item with same id', () => {
    const issue = {
      id: 12,
      title: 'Super test',
    };

    mutations.REPLACE_ISSUE(state, issue);
    expect(state.issues.length).toBe(4);
    expect(state.issues[0]).toBe(issue);
  });

  it('REPLACE ISSUE mutation, should leave state untouched, if no id is matching', () => {
    const issue = {
      id: 55,
      title: 'Super test',
    };

    const issuesBackup = [...state.issues];
    mutations.REPLACE_ISSUE(state, issue);
    expect(state.issues).toEqual(issuesBackup);
  });

  it('should SET_ISSUES mutations set issues', () => {
    const issues = [{
      id: 55,
      title: 'Super test',
    }];

    mutations.SET_ISSUES(state, issues);
    expect(state.issues).toBe(issues);
  });

  it('should SET_ISSUE_PROP change issue prop', () => {
    const data = {
      prop: 'title',
      value: 'New title!',
    };

    expect(state.issue.title).toBe('Issue 1');
    mutations.SET_ISSUE_PROP(state, data);
    expect(state.issue.title).toBe(data.value);
  });

  it('should SET_ISSUE set issue', () => {
    const issue = {
      id: 55,
      title: 'Super test',
    };
    mutations.SET_ISSUE(state, issue);
    expect(state.issue).toEqual(issue);
  });

  it('should REMOVE_ISSUE remove issues ;)', () => {
    const issue = {
      id: 12,
      title: 'Super test',
    };
    expect(state.issues.length).toBe(4);
    mutations.REMOVE_ISSUE(state, issue);
    expect(state.issues.length).toBe(3);
  });

  it('should SET_FILTER_VALUE mutation set filter value to lowercased parameter', () => {
    mutations.SET_FILTER_VALUE(state, 'FiLtEr VaLue2');
    expect(state.filterValue).toBe('filter value2');
  });
});
