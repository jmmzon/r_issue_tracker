import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import { shallowMount } from '@vue/test-utils';
import IssueModal from '~client/components/IssueModal';
import VeeValidate from 'vee-validate';

Vue.use(BootstrapVue);
Vue.use(VeeValidate, {
  fieldsBagName: 'veeFields',
});

let wrapper;
let storeMock;

function mount() {
  wrapper = shallowMount(IssueModal, {
    propsData: {
      visible: true,
    },
    mocks: {
      $store: storeMock,
    },
    stubs: {
      BButton: '<button></button>',
    },
  });
}


describe('tests', () => {
  beforeEach(() => {
    storeMock = {
      state: {
        issue: {
          title: 'Title 2',
        },
      },
      dispatch: jest.fn(),
    };
    storeMock.dispatch.mockResolvedValue(true);
    mount();
  });

  it('modal should match snapshot', () => {
    expect(wrapper.element).toMatchSnapshot();
  });

  it('title input should exists, and when changes dispatch action', () => {
    const titleInput = wrapper.find('#new-issue-name');
    expect(titleInput.exists()).toBe(true);
    titleInput.vm.$emit('input', 'xdd');
    expect(storeMock.dispatch).toHaveBeenCalledWith('setIssueProp', { prop: 'title', value: 'xdd' });
  });

  it('modal title should be set to "Create new issue" when issue id is not set', () => {
    expect(wrapper.vm.modalTitle).toBe('Create new issue');
  });

  it('modal title should be set to "Edit issue" when issue id is provided', () => {
    storeMock.state.issue.id = 12;
    mount();
    expect(wrapper.vm.modalTitle).toBe('Edit issue');
  });

  it('should dispatch createIssueAction & call closeWindow method when submit button is clicked', async () => {
    const closeModal = jest.fn();
    wrapper.setMethods({
      closeModal,
    });
    const submitButton = wrapper.find('#new-issue-submit-form-button');

    expect(submitButton.exists()).toBe(true);

    submitButton.vm.$emit('click');
    await Vue.nextTick();

    expect(storeMock.dispatch).toHaveBeenCalledWith('createIssue', storeMock.state.issue);
    expect(closeModal).toHaveBeenCalled();
  });
});
