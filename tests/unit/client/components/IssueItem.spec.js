import { mount as _mount } from '@vue/test-utils';
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import IssueItem from '~client/components/IssueItem';
import { STATUS_OPEN, STATUS_PENDING } from '~common/lib/statuses';

Vue.use(BootstrapVue);

const mockStore = { };

const openedIssue = {
  id: 1,
  title: 'Opened issue',
  status_id: STATUS_OPEN,
};

const pendingIssue = {
  id: 2,
  title: 'Pending issue',
  status_id: STATUS_PENDING,
};

let wrapper;

function mount(options) {
  wrapper = _mount(IssueItem, {
    ...options,
    mocks: {
      $store: mockStore,
    },
  });
}

describe('IssueItem.vue test', () => {
  beforeEach(() => {
    mockStore.dispatch = jest.fn();
  });

  it('should have set pending button if is open issue', () => {
    mount({
      propsData: {
        issue: openedIssue,
      },
    });

    const pendingButton = wrapper.find('#set-pending-button');
    const closeButton = wrapper.find('#set-closed-button');

    pendingButton.trigger('click');

    expect(wrapper.vm.allowSetPending).toBe(true);
    expect(wrapper.vm.allowClose).toBe(false);
    expect(wrapper.element).toMatchSnapshot();
    expect(pendingButton.exists()).toBe(true);
    expect(closeButton.exists()).toBe(false);
    expect(mockStore.dispatch).toHaveBeenCalledWith('setPending', openedIssue.id);
  });

  it('should have set close button if is pending issu', () => {
    mount({
      propsData: {
        issue: pendingIssue,
      },
    });

    expect(wrapper.vm.allowSetPending).toBe(false);
    expect(wrapper.vm.allowClose).toBe(true);
    expect(wrapper.element).toMatchSnapshot();
    const pendingButton = wrapper.find('#set-pending-button');
    const closeButton = wrapper.find('#set-closed-button');
    closeButton.trigger('click');
    expect(pendingButton.exists()).toBe(false);
    expect(closeButton.exists()).toBe(true);
    expect(mockStore.dispatch).toHaveBeenCalledWith('setClosed', pendingIssue.id);
  });
});
