
import { Builder } from '~client/libs/IssueData';

describe('IssueData & Builder tests', () => {
  it('should build correctly new object', () => {
    const { obj } = Builder.from({ name: 'Jasiek' })
      .set('id', 12)
      .set('name', null)
      .set('firstName');

    expect(obj).toEqual({
      id: 12,
      name: 'Jasiek',
    });
  });
});
