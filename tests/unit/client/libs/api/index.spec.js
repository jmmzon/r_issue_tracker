
import axios from 'axios';

// eslint-disable-next-line no-unused-vars
import api from '~client/libs/api';

jest.mock('axios');


it('should create axios isntance', () => {
  expect(axios.create).toBeCalledWith({
    baseURL: expect.any(String),
  });
});
