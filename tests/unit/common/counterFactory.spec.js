import counterFactory from '~common/counterFactory';

describe('counter  test', () => {
  it('counter should generate growing values starting from zero', () => {
    const counter = counterFactory();

    expect(counter.next().value).toBe(0);
    expect(counter.next().value).toBe(1);
    expect(counter.next().value).toBe(2);
    expect(counter.next().value).toBe(3);
  });
});
