import {
  canChangeStatus,
  STATUS_OPEN,
  STATUS_CLOSED,
  STATUS_PENDING,
  STATUS_REMOVED,
} from '~common/lib/statuses/index';


describe('statues transitions tests', () => {
  it('should canChangeStatus for STATUS_OPEN => STATUS_PENDING be true', () => {
    expect(canChangeStatus({ status_id: STATUS_OPEN }, STATUS_PENDING)).toBe(true);
  });
  it('should canChangeStatus for STATUS_OPEN => STATUS_CLOSED be false', () => {
    expect(canChangeStatus({ status_id: STATUS_OPEN }, STATUS_CLOSED)).toBe(false);
  });
  it('should canChangeStatus for STATUS_OPEN => STATUS_REMOVED be true', () => {
    expect(canChangeStatus({ status_id: STATUS_OPEN }, STATUS_REMOVED)).toBe(true);
  });
  it('should canChangeStatus for STATUS_PENDING => STATUS_OPEN be false', () => {
    expect(canChangeStatus({ status_id: STATUS_PENDING }, STATUS_OPEN)).toBe(false);
  });
  it('should canChangeStatus for STATUS_PENDING => STATUS_CLOSED be true', () => {
    expect(canChangeStatus({ status_id: STATUS_PENDING }, STATUS_CLOSED)).toBe(true);
  });
  it('should canChangeStatus for STATUS_PENDING => STATUS_REMOVED be false', () => {
    expect(canChangeStatus({ status_id: STATUS_PENDING }, STATUS_REMOVED)).toBe(false);
  });
  it('should canChangeStatus for STATUS_CLOSED => STATUS_OPEN be false', () => {
    expect(canChangeStatus({ status_id: STATUS_CLOSED }, STATUS_OPEN)).toBe(false);
  });
  it('should canChangeStatus for STATUS_CLOSED => STATUS_PENDING be false', () => {
    expect(canChangeStatus({ status_id: STATUS_CLOSED }, STATUS_PENDING)).toBe(false);
  });
  it('should canChangeStatus for STATUS_CLOSED => STATUS_REMOVED be false', () => {
    expect(canChangeStatus({ status_id: STATUS_CLOSED }, STATUS_REMOVED)).toBe(false);
  });
});
