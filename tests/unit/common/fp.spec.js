import {
  groupBy,
  keyBy,
  not,
  isEmpty,
  whereProp,
  isFunction,
} from '~common/fp';

const users = [{
  id: 15,
  first_name: 'Jan',
  last_name: 'Zon',
}, {
  id: 22,
  first_name: 'Piotr',
  last_name: 'Kowalski',
}, {
  id: 15,
  first_name: 'Mikołaj',
  last_name: 'Jurek',
}];


describe('fp (functional programming) helpers tests', () => {
  it('should `not`-ted function returns negate result', () => {
    const fn = val => val;
    const notFn = not(fn);

    expect(fn(true)).toBe(true);
    expect(notFn(true)).toBe(false);
    expect(notFn(false)).toBe(true);
  });

  it('should keyBy returns users keyed by id', () => {
    const usersKeyed = keyBy(users, 'id');

    expect(usersKeyed).toEqual({
      15: users[2],
      22: users[1],
    });
  });

  it('should groupBy returns users grouped by id', () => {
    const usersGrouped = groupBy(users, 'id');
    expect(usersGrouped).toEqual({
      15: [
        users[0],
        users[2],
      ],
      22: [
        users[1],
      ],
    });
  });

  it('isEmpty should return true for empty array', () => {
    expect(isEmpty([])).toBe(true);
  });

  it('isEmpty should return false for non empty array', () => {
    expect(isEmpty([1, 2, 3])).toBe(false);
  });

  it('isEmpty should return true for empty object', () => {
    expect(isEmpty({})).toBe(true);
  });

  it('isEmpty should return true for non empty object', () => {
    expect(isEmpty({ id: 12 })).toBe(false);
  });

  it('whereProp should return true, for correct object property value', () => {
    expect(whereProp('id', 15)(users[0])).toBe(true);
  });

  it('whereProp should return false, for incorrect object property value', () => {
    expect(whereProp('id', 22)(users[0])).toBe(false);
  });

  it('isFunction should return true, for function parameter', () => {
    expect(isFunction(Math.sin)).toBe(true);
  });

  it('isFunction should return false, for function parameter', () => {
    expect(isFunction(Array.prototype.isArray)).toBe(false);
  });
});
