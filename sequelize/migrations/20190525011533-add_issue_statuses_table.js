

module.exports = {
  /**
   * @param {QueryInterface} queryInterface
   * @param {sequelize.DataTypes} Sequelize
   */
  up: (queryInterface, Sequelize) => queryInterface.createTable('issue_statuses', {
    id: {
      type: Sequelize.INTEGER(10).UNSIGNED,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: Sequelize.STRING(80),
      allowNull: false,
    },
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE,
  }),
  /**
   * @param {QueryInterface} queryInterface
   */
  down: queryInterface => queryInterface.dropTable('issue_statuses'),
};
