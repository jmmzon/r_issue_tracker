

module.exports = {
  /**
   * @param {QueryInterface} queryInterface
   */
  up: queryInterface => queryInterface.bulkInsert('issue_statuses', [
    {
      id: 1,
      name: 'Opened',
    },
    {
      id: 2,
      name: 'Pending',
    },
    {
      id: 3,
      name: 'Closed',
    },
  ]),
  /**
   * @param {QueryInterface} queryInterface
   */
  down: queryInterface => queryInterface.bulkDelete('issue_statuses', {
    where: {
      id: [1, 2, 3],
    },
  }),
};
