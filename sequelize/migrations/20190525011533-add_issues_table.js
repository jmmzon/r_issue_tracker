

module.exports = {
  /**
   * @param {QueryInterface} queryInterface
   * @param {sequelize.DataTypes} Sequelize
   */
  up: (queryInterface, Sequelize) => queryInterface.createTable('issues', {
    id: {
      type: Sequelize.INTEGER(10).UNSIGNED,
      primaryKey: true,
      autoIncrement: true,
    },
    status_id: {
      type: Sequelize.INTEGER(10).UNSIGNED,
      allowNull: false,
      references: {
        model: 'issue_statuses',
        key: 'id',
      },
    },
    title: {
      type: Sequelize.STRING(80),
      allowNull: false,
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: false,
      defaultValue: '',
    },
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE,
  }),
  /**
   * @param {QueryInterface} queryInterface
   */
  down: queryInterface => queryInterface.dropTable('issues'),
};
